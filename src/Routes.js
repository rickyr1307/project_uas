import React, { Component } from 'react';
import { Router, Stack, Scene } from 'react-native-router-flux';
import Login from './pages/Login';
import Signup from './pages/Signup';
import Forget from './pages/Forget';
import Dashboard from './pages/Dashboard'
import Tugas from './pages/Tugas'
import Transkrip from './pages/Transkrip'
import Jadwal from './pages/Jadwal'
import Pembayaran from './pages/Pembayaran'
import Notif from './pages/Notif'
import News from './pages/News'
import Profile from './pages/Profile'
import Absen from './pages/Absen'

export default class Routes extends Component {
    render() {
        return (
            <Router>
                <Stack key="root" hideNavBar={true}>
                    <Scene key="login" component={Login} title="Login" initial={true} />
                    <Scene key="signup" component={Signup} title="Register" />
                    <Scene key="forget" component={Forget} title="Forget" />
                    <Scene key="dashboard" component={Dashboard} title="Dashboard" />
                    <Scene key="tugas" component={Tugas} title="Tugas" />
                    <Scene key="transkrip" component={Transkrip} title="Transkrip" />
                    <Scene key="jadwal" component={Jadwal} title="Jadwal" />
                    <Scene key="pembayaran" component={Pembayaran} title="Pembayaran" />
                    <Scene key="notif" component={Notif} title="Notif" />
                    <Scene key="news" component={News} title="News" />
                    <Scene key="profile" component={Profile} title="Profile" />
                    <Scene key="absen" component={Absen} title="Absen" />

                </Stack>
            </Router>
        )
    }
};