import React, { Component } from 'react';
import { View, TouchableHighlight, Text, StyleSheet, Image, AppRegistry, Platform } from 'react-native';

import PropTypes from 'prop-types';

class SelectedArray {
    constructor() {
        selectedItemsArray = [];
    }

    setItem(option) {
        selectedItemsArray.push(option);
    }

    getArray() {
        return selectedItemsArray;
    }
}

class Checkbox extends Component {
    constructor() {
        super();

        this.state = { checked: null }
    }

    componentWillMount() {
        if (this.props.checked) {
            this.setState({ checked: true }, () => {
                this.props.selectedArrayObject.setItem({ 'key': this.props.keyValue, 'label': this.props.label });
            });
        }
        else {
            this.setState({ checked: false });
        }
    }

    toggleState(key, label) {
        this.setState({ checked: !this.state.checked }, () => {
            if (this.state.checked) {
                this.props.selectedArrayObject.setItem({ 'key': key, 'label': label });
            }
            else {
                this.props.selectedArrayObject.getArray().splice(this.props.selectedArrayObject.getArray().findIndex(x => x.key == key), 1);
            }
        });
    }

    render() {
        return (
            <TouchableHighlight onPress={this.toggleState.bind(this, this.props.keyValue, this.props.label)} underlayColor="transparent" style={styles.checkBoxButton}>
                <View style={styles.checkBoxHolder}>
                    <View style={{ width: this.props.size, height: this.props.size, backgroundColor: this.props.color, padding: 2 }}>
                        {
                            (this.state.checked)
                                ?
                                (<View style={styles.checkedView}>
                                    <Image source={require('./../images/Barcode.png')} style={styles.checkedImage} />
                                </View>)
                                :
                                (<View style={styles.uncheckedView} />)
                        }
                    </View>
                    <Text style={[styles.checkBoxLabel, { color: this.props.color }]}>{this.props.label}</Text>
                </View>
            </TouchableHighlight>
        );
    }
}

class CheckBox extends Component {
    constructor() {
        super();

        selectedArrayRef = new SelectedArray();
    }

    getSelectedItems = () => {
        if (selectedArrayRef.getArray().length == 0) {
            alert('No Item(s) Selected!');
        }
        else {
            console.log(selectedArrayRef.getArray());
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Checkbox size={20} keyValue={1} selectedArrayObject={selectedArrayRef} checked={false} color="#636c72" label="Item #1" />
                <Checkbox size={20} keyValue={2} selectedArrayObject={selectedArrayRef} checked={false} color="#0275d8" label="Item #2" />
                <Checkbox size={20} keyValue={3} selectedArrayObject={selectedArrayRef} checked={false} color="#5cb85c" label="Item #3" />
                <Checkbox size={20} keyValue={4} selectedArrayObject={selectedArrayRef} checked={false} color="#f0ad4e" label="Item #4" />
                {/* <Checkbox size={20} keyValue={5} selectedArrayObject={selectedArrayRef} checked={true} color="#d9534f" label="Item #5" /> */}

                <TouchableHighlight underlayColor="rgba(0,0,0,0.6)" style={styles.selectedArrayItemsBtn} onPress={this.getSelectedItems}>
                    <Text style={styles.btnText}>Get Selected</Text>
                </TouchableHighlight>
            </View>
        );
    }
}

const styles = StyleSheet.create(
    {
        container:
        {
            marginTop: 10,
            paddingHorizontal: 25,
            justifyContent: 'center',
            alignItems: 'center'
        },

        selectedArrayItemsBtn:
        {
            marginTop: 20,
            padding: 5,
            backgroundColor: 'rgba(0,0,0,0.6)',
            alignSelf: 'stretch'
        },

        btnText:
        {
            color: 'white',
            textAlign: 'center',
            alignSelf: 'stretch',
            fontSize: 18
        },

        checkBoxButton:
        {
            marginVertical: 5
        },

        checkBoxHolder:
        {
            flexDirection: 'row',
            alignItems: 'center'
        },

        checkedView:
        {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
        },

        checkedImage:
        {
            height: '80%',
            width: '80%',
            tintColor: 'white',
            resizeMode: 'contain'
        },

        uncheckedView:
        {
            flex: 1,
            backgroundColor: 'white'
        },

        checkBoxLabel:
        {
            fontSize: 17,
            paddingLeft: 10
        }
    });

Checkbox.propTypes =
{
    size: PropTypes.number,
    keyValue: PropTypes.number.isRequired,
    selectedArrayObject: PropTypes.object.isRequired,
    color: PropTypes.string,
    label: PropTypes.string,
    checked: PropTypes.bool
}

Checkbox.defaultProps =
{
    size: 30,
    color: '#636c72',
    label: 'Default',
    checked: false
}

export default CheckBox