import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import SegmentedControlTab from 'react-native-segmented-control-tab'
import { Actions } from 'react-native-router-flux'

export default class Tabs extends Component {
    constructor() {
        super()
        this.state = {
            selectedIndex: 0,
            modalVisible: false
        };
    }

    handleIndexChange = (index) => {
        this.setState({
            ...this.state,
            selectedIndex: index,
        });
    }

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', paddingHorizontal: 40 }}>
                <SegmentedControlTab
                    values={['Tagihan', 'Lunas']}
                    selectedIndex={this.state.selectedIndex}
                    onTabPress={Actions.transaksi}
                    activeTabStyle={styles.activeTabStyle}
                    tabStyle={styles.tabStyle}
                    tabTextStyle={styles.tabTextStyle}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffff',
    },
    activeTabStyle: {
        backgroundColor: '#19769F'
    },
    tabStyle: {
        borderColor: '#19769F',
        marginTop: 35,
        height: 40,
    },
    tabTextStyle: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#51565F'
    },
    navbar: {
        flex: 1,
        marginTop: 10,
        borderWidth: 1,
        borderColor: '#ffff',
    }
})