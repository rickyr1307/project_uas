import React from 'react'
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native'
import { Icon } from 'react-native-elements'
import { Actions } from 'react-native-router-flux'




const Menu = ({ }) => {
    return (
        <View style={styles.container}>
            <View style={styles.menu}>
                <TouchableOpacity style={styles.button} onPress={Actions.tugas}>
                    <Icon name="work" color='#5FE5BC' size={55} />
                    <Text style={styles.buttonText}>Tugas</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={Actions.jadwal}>
                    <Icon name="calendar-week" color='#5FE5BC' type='material-community' size={55} />
                    <Text style={styles.buttonText}>Jadwal Perkuliahan</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={Actions.pembayaran}>
                    <Icon name="wallet" color='#5FE5BC' size={55} type='material-community' />
                    <Text style={styles.buttonText}>Pembayaran</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={Actions.transkrip}>
                    <Icon name="graduation-cap" color='#5FE5BC' size={55} type='font-awesome' />
                    <Text style={styles.buttonText}>Transkrip Nilai</Text>
                </TouchableOpacity>

            </View>
        </View >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'transparent'
    },
    menu: {
        flex: 1,
        marginLeft: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginVertical: 5,
        justifyContent: 'center'
    },
    button: {
        borderWidth: 1,
        borderColor: '#5FE5BC',
        width: "45%",
        borderRadius: 10,
        height: "45%",
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10,
        marginTop: 10,
    },
    buttonText: {
        marginVertical: 5,
        color: '#3D3D3D'

    }
})

export default Menu