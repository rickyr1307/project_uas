import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button'
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';


export default class Radio extends Component {


    constructor() {
        super()
        this.state = {
            text: ''
        }
        this.onSelect = this.onSelect.bind(this)
    }

    copy = () => {
        alert('Copied To Keyboard !')
    }

    onSelect(index, value) {
        this.setState({
            text: `${value}`
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <View>

                    <RadioGroup
                        thickness={3}
                        onSelect={(index, value) => this.onSelect(index, value)}
                    >
                        <RadioButton color='#30C7A5' value={'BCA : 1122334455'} style={{ alignItems: 'center' }} >
                            <Image source={require('../images/BCA_logo.png')} style={{ marginLeft: 5 }} />
                        </RadioButton>
                        <RadioButton color='#30C7A5' value={'BRI : 0099887766'} style={{ alignItems: 'center', marginTop: 10 }} >
                            <Image source={require('../images/BRI_logo.png')} style={{ marginLeft: 5 }} />
                        </RadioButton>
                        <RadioButton color='#30C7A5' value={'ALFAMART : 16788261'} style={{ alignItems: 'center' }} >
                            <Image source={require('../images/ALFAMART_LOGO_BARU.png')} />
                        </RadioButton>
                        <RadioButton color='#30C7A5' value={'INDOMART : 17265418'} style={{ alignItems: 'center' }} >
                            <Image source={require('../images/indomart.png')} style={{ marginLeft: 5 }} />
                        </RadioButton>

                    </RadioGroup>
                </View>
                <View style={styles.resi}>
                    <Text style={styles.text}>{this.state.text}</Text>
                </View>
                <View style={{ alignItems: 'center', marginTop: 10 }} >
                    <TouchableOpacity style={styles.button} onPress={this.copy}>
                        <Text style={{ color: '#fff', fontWeight: 'bold' }}>Copy</Text>
                    </TouchableOpacity>
                </View>
            </View >
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    resi: {
        marginHorizontal: 15,
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 13,
        borderColor: '#1B7EA0',
        height: 40,
        marginTop: 10,
    },
    text: {
        justifyContent: 'center',
        marginTop: 8,
        color: '#19769F',
        fontSize: 14,
        fontFamily: 'GothamRounded'
    },
    button: {
        borderRadius: 5,
        width: 129,
        height: 41,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        marginBottom: 20,
        borderColor: '#fff',
        backgroundColor: '#2DBCA4',
        elevation: 5
    },

});