import React from 'react'
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native'
import { Icon } from 'react-native-elements'
import { Actions } from 'react-native-router-flux'



const Navbar = ({ }) => {

    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={Actions.dashboard}>
                <Icon color="#A8A8A8" name="home" size={40} type='font-awesome' />
            </TouchableOpacity>
            <TouchableOpacity onPress={Actions.absen}>
                <Icon color="#A8A8A8" name="fingerprint" size={40} type='material' />
            </TouchableOpacity>
            <TouchableOpacity onPress={Actions.profile}>
                <Icon color="#A8A8A8" name="user-circle" size={35} type='font-awesome' />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        alignContent: 'stretch',
        flexDirection: 'row',
        marginHorizontal: 20,
        margin: 5
    },
    icon: {

    }

})



export default Navbar