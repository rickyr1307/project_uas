import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'

const Button = ({ text, press }) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={press}>
                <LinearGradient
                    start={{ x: 0.0, y: 0.25 }}
                    end={{ x: 0.5, y: 1.0 }}
                    locations={[0, 1]}
                    colors={['#1C84A5', '#36D5A4']}
                    style={styles.linearGradient}>
                    <Text style={styles.buttonText}>
                        {text}
                    </Text>
                </LinearGradient>
            </TouchableOpacity>
        </View >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    linearGradient: {
        borderRadius: 5,
        width: 129,
        height: 41,
        justifyContent: 'center',
        shadowColor: '#000000',
        shadowOpacity: 2,
        elevation: 6,
        shadowRadius: 15,
        shadowOffset: { width: 5, height: 13 },

    },
    buttonText: {
        fontSize: 14,
        color: '#ffffff',
        textAlign: 'center',
        fontFamily: 'Gotham Rounded'
    }
})



export default Button