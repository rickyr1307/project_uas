import React from 'react'
import { View, TextInput, StyleSheet } from 'react-native'

const InputText2 = ({ text, hldClr, slcClr, type, onClick, secure }) => {
    return (
        <View style={styles.container}>
            <TextInput style={styles.inputBox}
                placeholder={text}
                placeholderTextColor={hldClr}
                selectionColor={slcClr}
                keyboardType={type}
                onSubmitEditing={onClick}
                secureTextEntry={secure}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputBox: {
        width: 300,
        backgroundColor: '#ffff',
        borderRadius: 5,
        paddingHorizontal: 16,
        fontSize: 12,
        borderWidth: 1,
        borderColor: '#D5D5D5',
        height: 40
    },
})

export default InputText2