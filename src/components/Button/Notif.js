import React from 'react'
import { StyleSheet, View, TouchableOpacity } from 'react-native'
import { Icon } from 'react-native-elements'
import { Actions } from 'react-native-router-flux'

const Notif = ({ name, size, color }) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={Actions.notif}>
                <Icon style={styles.Notif}
                    name={name}
                    size={size}
                    color={color}

                />
            </TouchableOpacity>
        </View >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
})

export default Notif