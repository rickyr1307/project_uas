import React from 'react'
import { StyleSheet, View } from 'react-native'
import { Icon } from 'react-native-elements'

const Tap = ({ name, size, color, type }) => {
    return (
        <View style={styles.container}>
            <Icon
                name={name}
                size={size}
                color={color}
                type={type}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 5

    }
})

export default Tap