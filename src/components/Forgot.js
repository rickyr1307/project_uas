import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native'

const Forgot = ({ forget, press }) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity>
                <Text style={styles.Ask}
                    onPress={press}
                >
                    {forget}
                </Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        marginRight: 30,
        marginBottom: 20
    },

    Ask: {
        color: '#19769F',
    }

})

export default Forgot