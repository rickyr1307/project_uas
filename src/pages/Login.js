import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity, clear } from 'react-native'
import InputText1 from '../components/InputText1'
import Button from '../components/Button'
import Forgot from '../components/Forgot'
import { Actions } from 'react-native-router-flux'

export default class Login extends Component {

    signup() {
        Actions.signup()
    }

    forget() {
        Actions.forget()
    }

    dashboard() {
        Actions.dashboard()
    }

    signin() {
        clear()
    }


    render() {

        return (
            <View style={styles.container}>
                <View style={styles.title}>
                    <Text style={styles.head}>Sign In</Text>
                </View>
                <View style={styles.form}>
                    <InputText1 style={styles.input} text='E-mail' />
                    <InputText1 style={styles.input} text='Password' secure={true} />
                    <Forgot style={styles.forget} forget='Forget Password ?' press={this.forget} />
                </View>
                <View style={styles.button}>
                    <Button text='Sign In' press={this.dashboard} />
                </View>
                <View style={styles.signupTextCont}>
                    <Text style={styles.signupText}>Don't have an Account?</Text>
                    <TouchableOpacity onPress={this.signup} clearButtonMode="always">
                        <Text style={styles.signupButton}> Create Account.</Text>
                    </TouchableOpacity>
                </View>
            </View >

        );
    }
};
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#ffffff',
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'center',
        flexDirection: 'column'

    },
    title: {
        flexBasis: 2,
        flexGrow: 1,
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
        flexDirection: 'column',
        marginLeft: 30,
        marginTop: 120,
        marginBottom: 15
    },
    head: {
        color: '#19769F',
        fontSize: 24,

    },
    form: {

    },
    input: {
        marginBottom: 5
    },
    forget: {
        marginVertical: 5
    },

    button: {
        flexBasis: 2,
        flexGrow: 1

    },

    signupTextCont: {
        flexGrow: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingVertical: 16,
        flexDirection: 'row',
    },

    signupText: {
        color: '#95989A',
        fontSize: 16
    },

    signupButton: {
        color: '#19769F',
        fontSize: 16,
        fontWeight: '500'
    }

});
