import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, ScrollView, Alert } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Icon } from 'react-native-elements'
import { Actions } from 'react-native-router-flux';


export default class News extends Component {
    goBack() {
        Actions.pop()
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.head}>
                    <LinearGradient
                        start={{ x: 0.3, y: 0.90 }}
                        end={{ x: 0.9, y: 1.0 }}
                        locations={[0, 1]}
                        colors={['#1C84A5', '#36D5A4']} style={styles.linearGradient}>
                        <TouchableOpacity onPress={this.goBack}>
                            <Icon name='keyboard-backspace' color='#fff' />
                        </TouchableOpacity>
                        <Text style={styles.title}>Berita</Text>
                        <TouchableOpacity>
                            <Icon name='sort' color='transparent' />
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
                <View style={styles.image}>
                    <Image style={{ flex: 1, maxHeight: 150, width: '100%', borderRadius: 10 }} source={require('../images/slide_2.jpg')} />
                    <Text style={styles.newsTitle}>ARS University Fokus Menjadi Universitas Unggul di Bidang Digital</Text>
                </View>
                <View style={styles.deskripsi}>
                    <ScrollView>
                        <Text style={{ fontSize: 12, color: '#95989A' }}>
                            Perubahan cepat yang terjadi dalam industri global membuat pihak akademisi terus berpacu melahirkan terobosan-terobosan dalam bidang riset dan keilmuan, terutama dalam bidang digital yang menjadi corong tumbuhnya revolusi industri 4.0 dewasa ini.
                            {'\n'}
                            {'\n'}
                            Melihat hal ini, Universitas Adirajasa Reswara Sanjaya (ARS University) bertekad menjadi universitas yang unggul di bidang digital. Revolusi industri 4.0 yang terus digaungkan oleh pemerintah semakin memicu ARS University unjuk menjadi kampus yang mandiri dan berdaya saing global.
                            {'\n'}
                            {'\n'}
                            Hal ini ditekankan pihak ARS University saat menggelar wisuda program pascasarjana dan sarjana angkatan ke-1 tahun akademik 2018/2019 di Hotel Haris Festival Citylink Jalan Peta, Bandung, Kamis (12/12/2019).
                            {'\n'}
                            {'\n'}
                            Rektor ARS University Dr H Purwadhi MPd didampingi Warek Akademik Dr A Rohendi SH MM mengatakan bahwa para lulusan ARS University telah dibekali soft skill yang menitikberatkan pada kemampuan individu mahasiswa agar bisa berkembang dan berkarier sesuai bidangnya masing-masing.
                            "Apalagi kini, era industri 4.0 telah berlangsung, kami perlu melalukan penyesuaian dengan kurikulum yang up to date sebagai universitas unggul dibidang digital.
                            {'\n'}
                            {'\n'}
                            Tentunya kami sudah membekali seluruh lulusan dengan kemampuan soft skill, serta metode pendidikan yang membangun manusia secara keseluruhan dengan mengembangkan semua potensinya seperti sosial emosi, intelektual, moral atau karakter, kreatifitas dan spiritual,” kata Purwadhi.
                            {'\n'}
                            {'\n'}
                            Dengan mengubah diri menjadi universitas unggul dibidang digital, kata dia, maka seluruh kurikulum juga mempergunakan sistem digital, seperti talent digital, entepreneur digital, bigdata dan komputer offting.
                            {'\n'}
                            {'\n'}
                            Untuk mengaktualisasikan diri sebagai universitas unggul dibidang digital, menurut Urwadhi, pihaknya telah bekerjasama dengan ITB dan sudah melakukan MoU untuk riset bersama di bidang digital sebagai pendampingnya.
                            {'\n'}
                            {'\n'}
                            Dalam wisuda tahun ini, lembaga yang lahir tahun 2000 bernama ARS Internasional dan pengelolaannya sempat diberikan kepada Universitas BSI Bandung dan kini telah kembali lagi ke ARS University dengan semangat baru, menggelar wisuda sebanyak 683 lulusan yang teridiri dari 10 program studi yakni program studi Pasca Sarjana (S2) 8 orang, Manajemen (S1) 114 orang, Akuntansi (S1) 72 orang, Teknik Informatika (S1) 123 orang, Sistem Informasi (S1) 173 orang, Ilmu Komunikasi (S1) 88 orang, DKV (S1) 37 orang, Fika (S1) 37, STP ARS (S1) 15 dan Akpar (S1) 14 orang.
                            {'\n'}
                            {'\n'}
                            Dalam wisudah ini, lulusan terbaik diraih oleh Gita Indriani, Prodi Akuntansi dengan Indek Prestasi Komulatif (IPK) 3.92, diikuti Gabriela Novriana Mau Pelun IPK 3,92 (Manajemen), Anggit Nur Iman IPK 3,91 (Teknik Informatika), Rissa Nurfitriana Handayani IPK 3,82 (Sistem Informasi), Muthia Alfianty IPK 3,75 (Ilmu Komunikasi), Warsa Suarsa IPK 3,71 (DKV), Fanny Agustina Hidayat IPK 3,74 (Manajemen STP ARS), Delia Wati Putri IPK 3,60 (Fika) dan Try Nanda Sari IPK 3,89 (Akpar BSI). (*)
                            </Text>
                    </ScrollView>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#ffff'
    },
    head: {
        flex: 1,
        maxHeight: 60
    },
    linearGradient: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 56,
        flex: 1,
        paddingHorizontal: 15
    },
    image: {
        flex: 3,
        padding: 15,
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#D5D5D5',
        marginBottom: 12
    },
    deskripsi: {
        flex: 5,
        paddingHorizontal: 10,

    },
    back: {
        color: '#fff'
    },
    title: {
        color: '#fff',
        fontSize: 20,
        alignItems: 'center',
        fontWeight: 'bold'
    },
    newsTitle: {
        color: '#19769F',
        fontWeight: 'bold',
        fontSize: 16,
        textAlign: 'center',
        marginTop: 12
    }
})