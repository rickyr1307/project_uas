import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, ScrollView, Alert } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Icon } from 'react-native-elements'
import { Actions } from 'react-native-router-flux'
import Navbar from '../compponent/Navbar'


export default class Tugas extends Component {
    goBack() {
        Actions.pop()
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.head}>
                    <LinearGradient
                        start={{ x: 0.3, y: 0.90 }}
                        end={{ x: 0.9, y: 1.0 }}
                        locations={[0, 1]}
                        colors={['#1C84A5', '#36D5A4']} style={styles.linearGradient}>
                        <TouchableOpacity onPress={this.goBack}>
                            <Icon name='keyboard-backspace' color='#fff' />
                        </TouchableOpacity>
                        <Text style={styles.title}>TUGAS</Text>
                        <TouchableOpacity>
                            <Icon name='keyboard-backspace' color='transparent' />
                        </TouchableOpacity>
                    </LinearGradient>
                </View>

                <View style={styles.menu}>
                    <ScrollView>
                        <View style={styles.tugas}>
                            <TouchableOpacity disabled>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 14, marginBottom: 14 }}>Mobile Programming</Text>
                                    <Text style={{ fontSize: 10, color: '#9D9D9D' }}>DeadLine : 25-11-2019</Text>
                                </View>
                                <View style={{ marginBottom: 30 }}>
                                    <Text style={{ fontSize: 12, color: '#9D9D9D', }}>Membuat MockUp untuk Nilai  UTS , minimal 5 halaman.</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.tugas}>
                            <TouchableOpacity disabled>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 14, marginBottom: 14 }}>Jaringan Komunikasi</Text>
                                    <Text style={{ fontSize: 10, color: '#9D9D9D' }}>DeadLine : 29-11-2019</Text>
                                </View>
                                <View style={{ marginBottom: 30 }}>
                                    <Text style={{ fontSize: 12, color: '#9D9D9D', }}>Membuat Topologi Bus menggunakan Cisco Packet Tracer.</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.tugas}>
                            <TouchableOpacity disabled>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 14, marginBottom: 14 }}>Statistika</Text>
                                    <Text style={{ fontSize: 10, color: '#9D9D9D' }}>DeadLine : 02-12-2019</Text>
                                </View>
                                <View style={{ marginBottom: 30 }}>
                                    <Text style={{ fontSize: 12, color: '#9D9D9D', }}>Membuat Makalah sesuai Judul Kelompok.</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.tugas}>
                            <TouchableOpacity disabled>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 14, marginBottom: 14 }}>Sistem Operasi</Text>
                                    <Text style={{ fontSize: 10, color: '#9D9D9D' }}>DeadLine : 07-12-2019</Text>
                                </View>
                                <View style={{ marginBottom: 30 }}>
                                    <Text style={{ fontSize: 12, color: '#9D9D9D', }}>Membuat Resume Jenis-jenis sitem operasi.</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.tugas}>
                            <TouchableOpacity disabled>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 14, marginBottom: 14 }}>Struktur Data</Text>
                                    <Text style={{ fontSize: 10, color: '#9D9D9D' }}>DeadLine : 08-12-2019</Text>
                                </View>
                                <View style={{ marginBottom: 30 }}>
                                    <Text style={{ fontSize: 12, color: '#9D9D9D', }}>Membuat Apikasi Pencari Angka Genap C++.</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.tugas}>
                            <TouchableOpacity disabled>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 14, marginBottom: 14 }}>Sistem Operasi</Text>
                                    <Text style={{ fontSize: 10, color: '#9D9D9D' }}>DeadLine : 12-12-2019</Text>
                                </View>
                                <View style={{ marginBottom: 30 }}>
                                    <Text style={{ fontSize: 12, color: '#9D9D9D', }}>Presentasi</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
                <View style={styles.navbar}>
                    <Navbar />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#ffff'
    },
    head: {
        flex: 1,
    },
    linearGradient: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 56,
        flex: 1,
        paddingHorizontal: 15
    },
    menu: {
        flex: 10,
        color: 'blue',
        // alignItems: 'center',
        marginTop: 10,
        marginLeft: 13
    },
    tugas: {
        borderBottomWidth: 1,
        width: "95%",
        borderBottomColor: '#9D9D9D',
        marginVertical: 10,

    },
    back: {
        color: '#fff'
    },
    title: {
        color: '#fff',
        fontSize: 20,
        alignItems: 'center',

    },
    navbar: {
        flex: 1,
        marginTop: 10,
        borderWidth: 1,
        borderColor: '#ffff',
    }
})