import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, ScrollView, Alert } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Icon } from 'react-native-elements'
import { Actions } from 'react-native-router-flux';
import { Card, CardItem, Body } from 'native-base'
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import { LocaleConfig } from 'react-native-calendars';

export default class Jadwal extends Component {
    goBack() {
        Actions.pop()
    }

    render() {

        LocaleConfig.locales['fr'] = {
            monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mey', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            dayNamesShort: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
            today: 'Aujourd\'hui'
        };
        LocaleConfig.defaultLocale = 'fr';

        return (
            <View style={styles.container}>
                <View style={styles.head}>
                    <LinearGradient
                        start={{ x: 0.3, y: 0.90 }}
                        end={{ x: 0.9, y: 1.0 }}
                        locations={[0, 1]}
                        colors={['#1C84A5', '#36D5A4']} style={styles.linearGradient}>
                        <TouchableOpacity onPress={this.goBack} >
                            <Icon name='keyboard-backspace' color='#fff' />
                        </TouchableOpacity>
                        <Text style={styles.title}>Jadwal Perkuliahan</Text>
                        <TouchableOpacity>
                            <Icon name='keyboard-backspace' color='transparent' />
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
                <View style={styles.foto}>
                    <Card style={styles.card}>
                        <Calendar
                            // Initially visible month. Default = Date()
                            current={'2020-01-20'}
                            // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                            minDate={'2020-12-1'}
                            // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                            maxDate={'2012-05-30'}
                            // Handler which gets executed on day press. Default = undefined
                            onDayPress={(day) => { console.log('selected day', day) }}
                            // Handler which gets executed on day long press. Default = undefined
                            monthFormat={'MMM yyyy'}
                            // Handler which gets executed when visible month changes in calendar. Default = undefined
                            style={{ borderRadius: 20 }}
                            theme={{
                                backgroundColor: '#ffffff',
                                calendarBackground: '#ffffff',
                                textSectionTitleColor: '#b6c1cd',
                                selectedDayBackgroundColor: '#00adf5',
                                selectedDayTextColor: '#ffffff',
                                todayTextColor: '#00adf5',
                                dayTextColor: '#000',
                                textDisabledColor: '#d9e1e8',
                                dotColor: '#00adf5',
                                selectedDotColor: '#ffffff',
                                arrowColor: '#9D9D9D',
                                // monthTextColor: 'blue',
                                indicatorColor: 'blue',
                                textDayFontFamily: 'Roboto_medium',
                                //  textMonthFontFamily: 'SegoeiUI',
                                textDayHeaderFontFamily: 'Roboto',
                                // textDayFontWeight: '100',
                                textMonthFontWeight: 'bold',
                                textDayHeaderFontWeight: 'bold',
                                textDayFontSize: 16,
                                textMonthFontSize: 20,
                                textDayHeaderFontSize: 16
                            }}
                            markingType={'custom'}
                            markedDates={{
                                '2020-01-01': {
                                    customStyles: {
                                        container: {
                                        },
                                        text: {
                                            color: '#5FE5BC',
                                            fontWeight: 'bold'
                                        },
                                    },
                                },
                            }}

                        />
                    </Card>
                    <Card style={styles.card2}>
                        <CardItem style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center' }}>
                            <Text style={{ width: '100 %', textAlign: 'center', fontSize: 13 }}>Statistika</Text>
                            <Text style={{ fontSize: 11, color: '#9D9D9D' }}>17:00 - 19:45</Text>
                        </CardItem>
                    </Card>
                    <Card style={{ marginTop: 10, marginBottom: 40, borderColor: '#19769F' }}>
                        <CardItem style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center' }}>
                            <Text style={{ width: '100 %', textAlign: 'center', fontSize: 13 }}>Mobile Programming</Text>
                            <Text style={{ fontSize: 11, color: '#9D9D9D' }}>19:45 - 21:15</Text>
                        </CardItem>
                    </Card>

                </View>

            </View >
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffff'
    },
    head: {
        flex: 1,

    },
    linearGradient: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 56,
        flex: 1,
        paddingHorizontal: 15,
        marginTop: -140
    },
    title: {
        color: '#fff',
        fontSize: 20,
        alignItems: 'center',
        marginLeft: 15
    },
    foto: {
        flex: 2,
        marginHorizontal: 15,

    },
    card: {
        flex: 1,
        marginTop: -120,
        borderRadius: 10
    },
    card2: {
        marginTop: 10,
        borderColor: '#19769F'
    },

})