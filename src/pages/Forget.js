import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import InputText1 from '../components/InputText1'
import Tap from '../components/Button/Icon'
import Button from '../components/Button'

import { Actions } from 'react-native-router-flux'

export default class Forget extends Component {
    goBack() {
        Actions.pop();
    }


    render() {
        return (
            <View style={styles.container}>
                <View style={styles.head}>
                    <TouchableOpacity onPress={this.goBack}>
                        <Tap name='keyboard-backspace' />
                    </TouchableOpacity>
                </View>
                <View style={styles.content}>
                    <Text style={styles.title}>Forget Password</Text>
                    <Text style={styles.text}>We just need your registered e-mail ID to send reset
link</Text>
                    <InputText1 text='E-mail' />
                </View>
                <View style={styles.button}>
                    <Button text='Sign Up' press={this.goBack} />
                </View>
            </View>

        )
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#ffff',
        flex: 1,
        flexDirection: 'column',
        width: '100%',
    },
    head: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        paddingLeft: 25,
        marginTop: 20
    },
    content: {
        flex: 1,
    },
    title: {
        fontSize: 24,
        color: '#19769F',
        paddingLeft: 30,
        paddingBottom: 20

    },
    text: {
        fontSize: 12,
        color: '#95989A',
        paddingHorizontal: 30,
        marginBottom: 10
    },
    button: {
        flex: 3,
        justifyContent: 'center',
        marginBottom: 30,
        marginTop: 15
    }
})