import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Icon } from 'react-native-elements'
import { Actions } from 'react-native-router-flux'
import SegmentedControlTab from 'react-native-segmented-control-tab'
import Navbar from '../compponent/Navbar'
import Modal from "react-native-modal"
import { Card } from "native-base"
import Radio from './../compponent/RadioButton'


export default class Pembayaran extends Component {
    constructor() {
        super()
        this.state = {
            selectedIndex: 0,
            modalVisible: false
        };
    }



    toogleModal = () => {
        this.setState({ modalVisible: !this.state.modalVisible })
    }

    handleIndexChange = (index) => {
        this.setState({
            ...this.state,
            selectedIndex: index,
        });
    }

    goBack() {
        Actions.pop()
    }


    render() {
        return (
            <View style={styles.container}>
                <View style={styles.head}>
                    <LinearGradient
                        start={{ x: 0.3, y: 0.90 }}
                        end={{ x: 0.9, y: 1.0 }}
                        locations={[0, 1]}
                        colors={['#1C84A5', '#36D5A4']} style={styles.linearGradient}>
                        <TouchableOpacity onPress={this.goBack} >
                            <Icon name='keyboard-backspace' color='#fff' />
                        </TouchableOpacity>
                        <Text style={styles.title}>Pembayaran Perkuliahan</Text>
                        <TouchableOpacity>
                            <Icon name='keyboard-backspace' color='transparent' />
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
                <View style={{ flex: 1, alignItems: 'center', paddingHorizontal: 40 }}>
                    <SegmentedControlTab
                        values={['Tagihan', 'Lunas']}
                        selectedIndex={this.state.selectedIndex}
                        onTabPress={this.handleIndexChange}
                        activeTabStyle={styles.activeTabStyle}
                        tabStyle={styles.tabStyle}
                        tabTextStyle={styles.tabTextStyle}
                    />
                </View>
                <View style={{ flex: 6, alignContent: 'center', paddingHorizontal: 50, marginTop: 50, marginBottom: 10 }}>

                    <LinearGradient
                        start={{ x: 0.3, y: 0.90 }}
                        end={{ x: 0.9, y: 1.0 }}
                        locations={[0, 1]}
                        colors={['#1C84A5', '#36D5A4']} style={styles.linearGradient2}>
                        <View style={{ flex: 1, borderBottomWidth: 1, borderColor: '#fff', justifyContent: 'flex-end', alignItems: 'center' }}>
                            <Text style={{ color: '#fff', fontSize: 36, fontWeight: 'bold' }}>Seminar</Text>
                            <Text style={{ color: '#fff', }}>CCNA</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ color: '#fff', fontSize: 20, marginTop: 50 }}>Rp. 200.000</Text>
                            <Text style={{ color: '#fff', fontSize: 16, marginTop: 50 }}>Exp : 05 - November - 2019</Text>
                        </View>
                        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                            <TouchableOpacity style={styles.button} onPress={this.toogleModal} >
                                <Text style={{ color: '#fff', fontWeight: 'bold' }}>Bayar</Text>
                            </TouchableOpacity>
                        </View>
                    </LinearGradient>

                </View>
                <View style={styles.navbar}>
                    <Navbar />
                </View>

                <Modal
                    animationIn='zoomInDown'
                    animationInTiming={1000}
                    animationOut='zoomOutUp'
                    animationOutTiming={1000}
                    backdropColor='#fff'
                    isVisible={this.state.modalVisible}
                    onBackdropPress={() => this.setState({ modalVisible: false })}>

                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Card style={{ width: 300, height: 370, borderRadius: 30 }}>
                            <View style={{ flex: 1, padding: 20 }}>
                                <Radio />
                            </View>
                        </Card>
                    </View>

                </Modal>

            </View >
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffff',
    },
    head: {
        flex: 1,
        justifyContent: 'flex-start'

    },
    linearGradient: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
        paddingHorizontal: 15,
    },
    linearGradient2: {
        alignItems: 'center',
        flex: 1,
        borderRadius: 10
    },
    button: {
        borderRadius: 5,
        width: 129,
        height: 41,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#fff',
        borderWidth: 1,
        marginBottom: 20
    },
    title: {
        color: '#fff',
        fontSize: 20,
        alignItems: 'center',
        marginLeft: 15
    },
    activeTabStyle: {
        backgroundColor: '#19769F'
    },
    tabStyle: {
        borderColor: '#19769F',
        marginTop: 35,
        height: 40,
    },
    tabTextStyle: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#51565F'
    },
    navbar: {
        flex: 1,
        marginTop: 10,
        borderWidth: 1,
        borderColor: '#ffff',
    }
})