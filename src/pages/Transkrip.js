import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, ScrollView, Alert } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Icon } from 'react-native-elements'
import { Actions } from 'react-native-router-flux';
import Navbar from '../compponent/Navbar'

export default class Transkrip extends Component {
    goBack() {
        Actions.pop()
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.head}>
                    <LinearGradient
                        start={{ x: 0.3, y: 0.90 }}
                        end={{ x: 0.9, y: 1.0 }}
                        locations={[0, 1]}
                        colors={['#1C84A5', '#36D5A4']} style={styles.linearGradient}>
                        <TouchableOpacity onPress={this.goBack}>
                            <Icon name='keyboard-backspace' color='#fff' />
                        </TouchableOpacity>
                        <Text style={styles.title}>TRANSKRIP NILAI</Text>
                        <TouchableOpacity>
                            <Icon name='sort' color='#fff' />
                        </TouchableOpacity>
                    </LinearGradient>
                </View>

                <View style={styles.menu}>
                    <ScrollView>
                        <View style={styles.tugas}>
                            <TouchableOpacity disabled>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 14, color: '#19769F', fontWeight: 'bold' }}>Pendidikan Pancasila</Text>
                                    <Text style={{ fontSize: 16, color: '#19769F', fontWeight: 'bold', marginRight: 20, marginTop: 10 }}>A</Text>
                                </View>
                                <View style={{ marginBottom: 20 }}>
                                    <Text style={{ fontSize: 12, color: '#9D9D9D', }}>101</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.tugas}>
                            <TouchableOpacity disabled>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 14, color: '#19769F', fontWeight: 'bold' }}>Bahasa Inggris I</Text>
                                    <Text style={{ fontSize: 16, color: '#19769F', fontWeight: 'bold', marginRight: 20, marginTop: 10 }}>A</Text>
                                </View>
                                <View style={{ marginBottom: 20 }}>
                                    <Text style={{ fontSize: 12, color: '#9D9D9D', }}>104</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.tugas}>
                            <TouchableOpacity disabled>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 14, color: '#19769F', fontWeight: 'bold' }}>Bahasa Inggris II</Text>
                                    <Text style={{ fontSize: 16, color: '#19769F', fontWeight: 'bold', marginRight: 20, marginTop: 10 }}>A</Text>
                                </View>
                                <View style={{ marginBottom: 20 }}>
                                    <Text style={{ fontSize: 12, color: '#9D9D9D', }}>105</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.tugas}>
                            <TouchableOpacity disabled>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 14, color: '#19769F', fontWeight: 'bold' }}>Pemrograman Berbasis Objek</Text>
                                    <Text style={{ fontSize: 16, color: '#19769F', fontWeight: 'bold', marginRight: 20, marginTop: 10 }}>A</Text>
                                </View>
                                <View style={{ marginBottom: 20 }}>
                                    <Text style={{ fontSize: 12, color: '#9D9D9D', }}>108</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.tugas}>
                            <TouchableOpacity disabled>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 14, color: '#19769F', fontWeight: 'bold' }}>Logika & Algoritma</Text>
                                    <Text style={{ fontSize: 16, color: '#19769F', fontWeight: 'bold', marginRight: 20, marginTop: 10 }}>A</Text>
                                </View>
                                <View style={{ marginBottom: 20 }}>
                                    <Text style={{ fontSize: 12, color: '#9D9D9D', }}>207</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.tugas}>
                            <TouchableOpacity disabled>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 14, color: '#19769F', fontWeight: 'bold' }}>Enterpreneurship</Text>
                                    <Text style={{ fontSize: 16, color: '#19769F', fontWeight: 'bold', marginRight: 20, marginTop: 10 }}>A</Text>
                                </View>
                                <View style={{ marginBottom: 20 }}>
                                    <Text style={{ fontSize: 12, color: '#9D9D9D', }}>272</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.tugas}>
                            <TouchableOpacity disabled>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 14, color: '#19769F', fontWeight: 'bold' }}>Matematika Diskrit</Text>
                                    <Text style={{ fontSize: 16, color: '#19769F', fontWeight: 'bold', marginRight: 20, marginTop: 10 }}>A</Text>
                                </View>
                                <View style={{ marginBottom: 20 }}>
                                    <Text style={{ fontSize: 12, color: '#9D9D9D', }}>742</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.tugas}>
                            <TouchableOpacity disabled>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 14, color: '#19769F', fontWeight: 'bold' }}>Multimedia</Text>
                                    <Text style={{ fontSize: 16, color: '#19769F', fontWeight: 'bold', marginRight: 20, marginTop: 10 }}>A</Text>
                                </View>
                                <View style={{ marginBottom: 20 }}>
                                    <Text style={{ fontSize: 12, color: '#9D9D9D', }}>797</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
                <View style={styles.navbar}>
                    <Navbar />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#ffff'
    },
    head: {
        flex: 1,
    },
    linearGradient: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 56,
        flex: 1,
        paddingHorizontal: 15
    },
    menu: {
        flex: 10,
        color: 'blue',
        // alignItems: 'center',
        marginTop: 10,
        marginLeft: 13
    },
    tugas: {
        borderBottomWidth: 1,
        width: "95%",
        borderBottomColor: '#9D9D9D',
        marginVertical: 10,

    },
    back: {
        color: '#fff'
    },
    title: {
        color: '#fff',
        fontSize: 20,
        alignItems: 'center',

    },
    navbar: {
        flex: 1,
        marginTop: 10,
        borderWidth: 1,
        borderColor: '#ffff',
    }
})