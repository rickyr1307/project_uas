import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity, ScrollView } from 'react-native'
import Tap from '../components/Button/Icon'
import InputText2 from '../components/InputText2'
import Button from '../components/Button'
import { Actions } from 'react-native-router-flux';

export default class Signup extends Component {
    goBack() {
        Actions.pop()
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.head}>
                    <TouchableOpacity onPress={this.goBack}>
                        <Tap style={styles.Tap} name='keyboard-backspace' />
                    </TouchableOpacity>
                    <Text style={styles.title}>Sign Up</Text>
                </View>
                <ScrollView>
                    <View style={styles.form}>
                        <Text style={styles.text}>Name</Text>
                        <InputText2 text='' />
                        <Text style={styles.text}>E-mail</Text>
                        <InputText2 text='' />
                        <Text style={styles.text}>No. HP</Text>
                        <InputText2 text='' type={'number-pad'} />
                        <Text style={styles.text}>Password</Text>
                        <InputText2 text='' secure={true} />
                        <Text style={styles.text}>Confirm Password</Text>
                        <InputText2 text='' secure={true} />
                    </View>
                    <View style={styles.button}>
                        <Button text='Sign Up' press={this.goBack} />
                    </View>
                </ScrollView>
            </View>
        );
    }
};



const styles = StyleSheet.create({
    container: {
        backgroundColor: '#ffff',
        flex: 1,
        flexDirection: 'column',
        width: '100%'
    },
    head: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        paddingLeft: 25,
        marginTop: 20
    },
    title: {
        fontSize: 24,
        color: '#19769F',
        marginLeft: 86,
    },
    text: {
        fontSize: 14,
        color: '#19769F',
        paddingLeft: 30,
        marginTop: 5
    },
    form: {
        flex: 3,
        paddingVertical: 30,
    },
    button: {
        flex: 1,
        paddingVertical: 20
    },
    Tap: {
        padding: 5
    }



});
