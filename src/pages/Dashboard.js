import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, ScrollView, Alert } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Actions } from 'react-native-router-flux'
import Notif from '../components/Button/Notif'
import Menu from '../compponent/menu'
import Navbar from '../compponent/Navbar'
import { DeckSwiper, Card, CardItem } from 'native-base'


const cards = [
    {
        name: 'ARS University Fokus Menjadi',
        title: 'Universitas Unggul Dibidang Digital',
        image: require('../images/slide_2.jpg'),
    },
    {
        name: 'Mahasiswa ARS University Kunjungi',
        title: 'Galamedia',
        image: require('../images/slide_1.jpg'),
    },
    {
        name: 'Pascasarjana ARS University Ikuti',
        title: 'Seminar Kebangsaan',
        image: require('../images/slide_3.jpg'),
    }
]

export default class Dashboard extends Component {
    goBack() {
        Actions.pop();
    }

    render() {


        return (
            <View style={styles.container}>
                <View style={styles.head}>
                    <LinearGradient
                        start={{ x: 0.3, y: 0.90 }}
                        end={{ x: 0.9, y: 1.0 }}
                        locations={[0, 1]}
                        colors={['#1C84A5', '#36D5A4']} style={styles.linearGradient}>
                        <Image style={styles.Image} source={require('../images/ars.png')} />
                        <Text style={styles.pageTitle}>Dashboard</Text>
                        <Notif name='notifications' color='#ffff' size={35} />
                    </LinearGradient>
                </View>
                <View style={styles.news}>
                    <DeckSwiper
                        dataSource={cards}
                        renderItem={item =>
                            <Card transparent style={{ borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
                                <TouchableOpacity onPress={Actions.news}>
                                    <CardItem cardBody>
                                        <Image style={{ height: 130, flex: 1, borderRadius: 10 }} source={item.image} />
                                    </CardItem>
                                </TouchableOpacity>
                                <CardItem style={{ flexWrap: 'wrap', maxHeight: 55, alignContent: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: '#19769F', fontSize: 16, fontWeight: 'bold' }}>{item.name}</Text>
                                    <Text style={{ color: '#19769F', fontSize: 16, fontWeight: 'bold' }}>{item.title}</Text>
                                </CardItem>
                            </Card>
                        }
                    />

                </View>
                <View style={styles.sub}>

                </View>
                <View style={styles.menu}>
                    <Menu />
                </View>
                <View style={styles.navbar}>
                    <Navbar />
                </View>
            </View>

        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#ffff'
    },
    linearGradient: {
        alignItems: 'center',
        flexDirection: 'row',
        height: 56,
        flex: 1,
        paddingHorizontal: 15
    },
    head: {
        flex: 1,
    },
    Image: {
        flex: 1,
        maxWidth: 31,
        height: 31,
    },
    pageTitle: {
        fontSize: 26,
        width: 260,
        textAlign: 'center',
        color: '#ffff',
        marginLeft: 7

    },
    news: {
        flex: 3,
        paddingVertical: 5,
        paddingHorizontal: 16,
    },
    menu: {
        flex: 5,
        color: 'blue',
        marginTop: 5
    },
    navbar: {
        flex: 1,
        marginTop: 10,

    },
    slide: {
        flex: 5,
        alignItems: 'center',
        justifyContent: 'center',

    },
})