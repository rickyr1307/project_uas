import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, ScrollView, Alert } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Icon } from 'react-native-elements'
import { Card, CardItem, Body } from 'native-base'
import { Actions } from 'react-native-router-flux'

export default class Profile extends Component {
    goBack() {
        Actions.pop()
    }

    render() {
        return (
            <View style={styles.container}>
                <LinearGradient
                    start={{ x: 0.3, y: 0.90 }}
                    end={{ x: 0.9, y: 1.0 }}
                    locations={[0, 1]}
                    colors={['#1C84A5', '#36D5A4']} style={styles.linearGradient}>
                    <View style={styles.head}>
                        <TouchableOpacity onPress={this.goBack}>
                            <Icon name='keyboard-backspace' color='#fff' />
                        </TouchableOpacity>
                        <Text style={styles.title}>Profile</Text>
                        <TouchableOpacity>
                            <Icon name='sort' color='transparent' />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.foto}>
                        <Card style={styles.card}>
                            <Image source={require('../images/Profile.png')} style={styles.image} />
                            <CardItem style={{ alignContent: 'center' }}>
                                <Body style={{ alignItems: 'center' }}>
                                    <Text style={styles.nama}>Ricky Rachman Sidiq</Text>
                                    <View style={{ marginTop: 10 }}>
                                        <View style={{ flexDirection: 'row', borderBottomWidth: 1, marginBottom: 5 }}>
                                            <View style={{ width: '40%', justifyContent: 'center' }}><Text style={styles.field1}>NIM</Text></View>
                                            <View style={{ width: '60%', justifyContent: 'center' }}><Text style={styles.field2}>17180014</Text></View>
                                        </View>
                                        <View style={{ flexDirection: 'row', borderBottomWidth: 1, marginBottom: 5 }}>
                                            <View style={{ width: '40%', justifyContent: 'center' }}><Text style={styles.field1}>Kelas</Text></View>
                                            <View style={{ width: '60%', justifyContent: 'center' }}><Text style={styles.field2}>17.3B.33</Text></View>
                                        </View>
                                        <View style={{ flexDirection: 'row', borderBottomWidth: 1, marginBottom: 5 }}>
                                            <View style={{ width: '40%', justifyContent: 'center' }}><Text style={styles.field1}>Program Studi</Text></View>
                                            <View style={{ width: '60%', justifyContent: 'center' }}><Text style={styles.field2}>Teknik Informatika</Text></View>
                                        </View>
                                        <View style={{ flexDirection: 'row', borderBottomWidth: 1, marginBottom: 5 }}>
                                            <View style={{ width: '40%', justifyContent: 'center' }}><Text style={styles.field1}>No.Hp</Text></View>
                                            <View style={{ width: '60%', justifyContent: 'center' }}><Text style={styles.field2}>081555922675</Text></View>
                                        </View>
                                        <View style={{ flexDirection: 'row', borderBottomWidth: 1, marginBottom: 5 }}>
                                            <View style={{ width: '40%', justifyContent: 'center' }}><Text style={styles.field1}>E-mail</Text></View>
                                            <View style={{ width: '60%', justifyContent: 'center' }}><Text style={styles.field2}>rickyrachmansidiq@gmail.com</Text></View>
                                        </View>
                                        <View style={{ flexDirection: 'row', borderBottomWidth: 1, marginBottom: 5 }}>
                                            <View style={{ width: '40%', justifyContent: 'center' }}><Text style={styles.field1}>Alamat</Text></View>
                                            <View style={{ width: '60%', justifyContent: 'center' }}><Text style={styles.field2}>Bandung</Text></View>
                                        </View>
                                        <View style={styles.edit}>
                                            <TouchableOpacity>
                                                <LinearGradient
                                                    start={{ x: 0.0, y: 0.25 }}
                                                    end={{ x: 0.5, y: 1.0 }}
                                                    locations={[0, 1]}
                                                    colors={['#1C84A5', '#36D5A4']}
                                                    style={styles.linearGradientB}>
                                                    <Text style={styles.buttonText}>Edit</Text>
                                                </LinearGradient>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{ marginTop: 15 }} onPress={Actions.login}>
                                                <LinearGradient
                                                    start={{ x: 0.0, y: 0.25 }}
                                                    end={{ x: 0.5, y: 1.0 }}
                                                    locations={[0, 1]}
                                                    colors={['#1C84A5', '#36D5A4']}
                                                    style={styles.linearGradientB}>
                                                    <Text style={styles.buttonText}>Log Out</Text>
                                                </LinearGradient>
                                            </TouchableOpacity>
                                        </View>

                                    </View>

                                </Body>
                            </CardItem>

                        </Card>
                    </View>
                </LinearGradient>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#ffff'
    },
    linearGradient: {
        flex: 1,
    },
    head: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 50,
        flex: 1,
        paddingHorizontal: 15,
        marginTop: 15
    },
    foto: {
        marginTop: 20,
        flex: 7,
        marginHorizontal: 15,
        marginBottom: 10
    },
    card: {
        flex: 1,
        paddingHorizontal: 5,
        alignItems: 'center',
        borderRadius: 5
    },
    image: {
        marginTop: -50,
    },
    nama: {
        color: '#19769F',
        fontSize: 20,
        marginTop: 10,

    },
    field1: {
        fontSize: 14,
        color: "#19769F",
        fontWeight: 'bold'
    },
    field2: {
        fontSize: 12,
        color: "#51565F",
        fontWeight: 'bold'
    },
    back: {
        color: '#fff'
    },
    title: {
        color: '#fff',
        fontSize: 20,
        alignItems: 'center',

    },
    linearGradientB: {
        borderRadius: 5,
        width: 129,
        height: 41,
        justifyContent: 'center',
        shadowColor: '#000000',
        shadowOpacity: 2,
        elevation: 6,
        shadowRadius: 15,
        shadowOffset: { width: 5, height: 13 }
    },
    edit: {
        marginTop: 60,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent'
    },
    buttonText: {
        fontSize: 14,
        color: '#ffffff',
        textAlign: 'center',
        fontFamily: 'Gotham Rounded'
    }
})