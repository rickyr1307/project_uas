import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, ScrollView, Alert } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Icon } from 'react-native-elements'
import { Actions } from 'react-native-router-flux'
import Navbar from '../compponent/Navbar'


export default class Transkrip extends Component {
    goBack() {
        Actions.pop()
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.head}>
                    <LinearGradient
                        start={{ x: 0.3, y: 0.90 }}
                        end={{ x: 0.9, y: 1.0 }}
                        locations={[0, 1]}
                        colors={['#1C84A5', '#36D5A4']} style={styles.linearGradient}>
                        <TouchableOpacity onPress={this.goBack}>
                            <Icon name='keyboard-backspace' color='#fff' />
                        </TouchableOpacity>
                        <Text style={styles.title}>Notifikasi</Text>
                        <TouchableOpacity>
                            <Icon name='sort' color='transparent' />
                        </TouchableOpacity>
                    </LinearGradient>
                </View>

                <View style={styles.menu}>
                    <ScrollView>
                        <View style={styles.tugas}>
                            <TouchableOpacity disabled>
                                <View style={{ justifyContent: 'space-between', marginVertical: 30 }}>
                                    <Text style={{ fontSize: 14, color: '#19769F', fontWeight: 'bold' }}>Pembayaran</Text>
                                    <Text style={{ fontSize: 12, color: '#95989A' }}>Seminar</Text>
                                    <Text style={{ fontSize: 12, color: '#9D9D9D', }}>Batas akhir : 2 hari lagi</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.tugas}>
                            <TouchableOpacity disabled>
                                <View style={{ justifyContent: 'space-between', marginVertical: 30 }}>
                                    <Text style={{ fontSize: 14, color: '#19769F', fontWeight: 'bold' }}>Dosen</Text>
                                    <Text style={{ fontSize: 12, color: '#95989A' }}>Pak Erfian</Text>
                                    <Text style={{ fontSize: 12, color: '#9D9D9D', }}>Hari ini Quiz</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.tugas}>
                            <TouchableOpacity disabled>
                                <View style={{ justifyContent: 'space-between', marginVertical: 30 }}>
                                    <Text style={{ fontSize: 14, color: '#19769F', fontWeight: 'bold' }}>BEM</Text>
                                    <Text style={{ fontSize: 12, color: '#95989A' }}>Bazzar</Text>
                                    <Text style={{ fontSize: 12, color: '#9D9D9D', }}>Tanggal 19-12-2019</Text>
                                </View>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.tugas}>
                            <TouchableOpacity disabled>
                                <View style={{ justifyContent: 'space-between', marginVertical: 30 }}>
                                    <Text style={{ fontSize: 14, color: '#19769F', fontWeight: 'bold' }}>Pembayaran</Text>
                                    <Text style={{ fontSize: 12, color: '#95989A' }}>Seminar Cisco</Text>
                                    <Text style={{ fontSize: 12, color: '#9D9D9D', }}>Batas akhir : 2 hari lagi</Text>
                                </View>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.tugas}>
                            <TouchableOpacity disabled>
                                <View style={{ justifyContent: 'space-between', marginVertical: 30 }}>
                                    <Text style={{ fontSize: 14, color: '#19769F', fontWeight: 'bold' }}>Pembayaran</Text>
                                    <Text style={{ fontSize: 12, color: '#95989A' }}>Seminar Enterpreneur</Text>
                                    <Text style={{ fontSize: 12, color: '#9D9D9D', }}>Batas akhir : 2 hari lagi</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
                <View style={styles.navbar}>
                    <Navbar />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#ffff'
    },
    head: {
        flex: 1,
    },
    linearGradient: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 56,
        flex: 1,
        paddingHorizontal: 15
    },
    menu: {
        flex: 10,
        color: 'blue',
        marginLeft: 20
    },
    tugas: {
        borderBottomWidth: 1,
        width: "95%",
        borderBottomColor: '#9D9D9D'

    },
    back: {
        color: '#fff'
    },
    title: {
        color: '#fff',
        fontSize: 20,
        alignItems: 'center',
    },
    navbar: {
        flex: 1,
        marginTop: 10,
        borderWidth: 1,
        borderColor: '#ffff',
    },
})