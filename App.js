import React, { Component } from 'react'
import { View, StyleSheet, StatusBar } from 'react-native'
import Routes from './src/Routes'



export default class App extends Component {
    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#19769F" barStyle="light-content" />
                <Routes />
            </View>
        );
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
});
